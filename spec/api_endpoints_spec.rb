# frozen_string_literal: true

require 'spec_helper'
require 'sync_test_helper'

module API
  module Sync
    class MockSequels < WzSalesforceSync::SyncRoutes
      config = { service: 'product_api', model_type: MockSequel, entity_class: API::Entities::Sync::MockSequel }
      init_default_routes(config)
      init_update_pdf_route(config)
    end
  end
end

RSpec.describe API::Sync::MockSequels do
  # Create 3 entites
  let(:create_entities) do
    [
      MockSequel.create(url_key: 'url_key_0', uuid: 'uuid_1'),
      MockSequel.create(url_key: 'url_key_1', uuid: 'uuid_2'),
      MockSequel.create(url_key: 'url_key_2', uuid: 'uuid_3')
    ]
  end
  let(:required_params) { %w[url_key] }

  helper = TestHelpers::Sync::API.new(
    model_type: MockSequel, spec_instance: self, resource_name: 'mock_sequels', app: API::Sync::MockSequels
  )
  helper.test_default_endpoints
  helper.test_pdf_update_endpoint
end
