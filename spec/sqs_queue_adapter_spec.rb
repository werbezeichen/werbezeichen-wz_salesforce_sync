# frozen_string_literal: true

require 'spec_helper'

RSpec.describe WzSalesforceSync::SqsQueueAdapter, type: :class do
  subject { described_class.new('queue') }

  let(:queue_url) { 'https://queue.amazonaws.com/123456789012/queue' }
  let(:create_queue_response) do
    <<-XML
      <CreateQueueResponse>
        <CreateQueueResult>
            <QueueUrl>#{queue_url}</QueueUrl>
        </CreateQueueResult>
        <ResponseMetadata>
            <RequestId>7a62c49f-347e-4fc4-9331-6e8e7a96aa73</RequestId>
        </ResponseMetadata>
      </CreateQueueResponse>
    XML
  end
  let(:send_message_response) do
    <<-XML
      <SendMessageResponse>
        <SendMessageResult>
            <MD5OfMessageBody>78e731027d8fd50ed642340b7c9a63b3</MD5OfMessageBody>
            <MD5OfMessageAttributes>3ae8f24a165a8cedc005670c81a27295</MD5OfMessageAttributes>
            <MessageId>5fea7756-0ea4-451a-a703-a558b933e274</MessageId>
        </SendMessageResult>
        <ResponseMetadata>
            <RequestId>27daac76-34dd-47df-bd01-1f6e873584a0</RequestId>
        </ResponseMetadata>
    </SendMessageResponse>
    XML
  end

  before do
    stub_request(:post, 'https://sqs.eu-central-1.amazonaws.com/')
      .with(
        body: 'Action=CreateQueue&QueueName=queue&Version=2012-11-05',
        headers: { Host: 'sqs.eu-central-1.amazonaws.com' }
      )
      .to_return(status: 200, body: create_queue_response, headers: { "Content-Type": 'application/xml' })

    stub_request(:post, queue_url)
      .with(
        body: 'Action=SendMessage&DelaySeconds=1&MessageBody=message&QueueUrl=' \
              'https%3A%2F%2Fqueue.amazonaws.com%2F123456789012%2Fqueue&Version=2012-11-05',
        headers: { Host: 'queue.amazonaws.com' }
      )
      .to_return(status: 200, body: send_message_response, headers: { "Content-Type": 'application/xml' })
  end

  it 'setup a queue object to send messages to SQS' do
    expect(subject.queue_url).to eq(queue_url)
  end

  it 'send_usecase_to_queue' do
    expect(subject.sqs_client)
      .to receive(:send_message)
      .with(
        {
          delay_seconds: 1,
          message_body: '{"usecase_name":"usecase","payload":"message"}',
          queue_url: queue_url
        }
      )
    subject.send_usecase_to_queue('usecase', 'message')
  end

  it 'send_worker_task_to_queue' do
    expect(subject.sqs_client)
      .to receive(:send_message)
      .with(
        {
          delay_seconds: 1,
          message_body: '{"workerTaskName":"task","options":"options"}',
          queue_url: queue_url
        }
      )
    subject.send_worker_task_to_queue('task', 'options')
  end

  context 'sqs queue disabled' do
    before do
      ENV['DISABLE_SQS_QUEUE'] = 'true'
    end
    it 'can be disabled with DISABLE_SQS_QUEUE' do
      expect(subject.sqs_client)
        .not_to receive(:send_message)
      subject.send_worker_task_to_queue('task', 'options')
    end
  end
end
