# frozen_string_literal: true

require 'spec_helper'
require 'rack/test'

module TestHelpers
  module Sync
    class API
      def initialize(config)
        @model_type = config[:model_type]
        @spec_instance = config[:spec_instance]
        @resource_name = config[:resource_name]
        @resource_name ||= (@model_type.to_s.underscore + 's')
        @api_path = "/#{@resource_name}"
        @app = config[:app]
      end

      def test_default_endpoints
        api_path = @api_path
        model_type = @model_type
        app = @app

        @spec_instance.describe "browser.post #{api_path}/bulk_get" do
          before do
            allow_any_instance_of(Aws::SQS::Client)
              .to receive(:create_queue)
              .and_return(OpenStruct.new(queue_url: 'queue_url'))
            allow_any_instance_of(WzSalesforceSync::SqsQueueAdapter).to receive(:send_usecase_to_queue)
          end

          it 'by ids' do
            entities = create_entities
            entities[0].save
            entities[1].save
            entities[2].salesforce_id = 'sync_sf_id_0'
            entities[2].save

            params = {
              field_name: 'id',
              field_values: [entities[0].id.to_s, 10_000, entities[1].id.to_s, entities[2].salesforce_id.to_s, '']
            }

            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.post "#{api_path}/bulk_get", params

            expect(browser.last_response.status).to eq(200)

            body = JSON.parse(browser.last_response.body)
            expect(body.size).to eq 2

            expect(body.map { |el| el['werbezeichen_sync_version'] }.compact.size).to eq body.size
            expect(body.map { |el| el['id'] }.include?(entities[0].id)).to eq(true)
            expect(body.map { |el| el['id'] }.include?(entities[1].id)).to eq(true)
            expect(body.map { |el| el['id'] }.include?(entities[2].id)).to eq(false)
          end

          it 'get unsynced entities' do
            entities = create_entities

            entities[0].updated_by = 'salesforce'
            entities[0].is_synced = true
            entities[0].sync_version = Time.now
            entities[0].should_not_set_sync_arguments = true
            entities[0].save

            entities[1].save

            entities[2].updated_by = nil
            entities[2].should_not_set_sync_arguments = true
            entities[2].save

            params = { field_name: 'is_synced', field_values: [false] }
            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.post "#{api_path}/bulk_get", params

            expect(browser.last_response.status).to eq(200)
            body = JSON.parse(browser.last_response.body)

            expect(body.size).to eq 2

            expect(body.map { |el| el['id'] }.include?(entities[0].id)).to eq(false)
            expect(body.map { |el| el['id'] }.include?(entities[1].id)).to eq(true)
            expect(body.map { |el| el['id'] }.include?(entities[2].id)).to eq(true)
          end
        end

        @spec_instance.describe "post #{api_path}/bulk_update" do
          before do
            allow_any_instance_of(Aws::SQS::Client)
              .to receive(:create_queue)
              .and_return(OpenStruct.new(queue_url: 'queue_url'))
            allow_any_instance_of(WzSalesforceSync::SqsQueueAdapter).to receive(:send_usecase_to_queue)
          end

          it 'insert 2' do
            params = {}
            params[:data] = [
              { salesforce_id: 'sf_id_0',
                updated_by: 'any',
                sync_version: DateTime.iso8601('2001-01-01T02:01:01+01:00'),
                is_synced: true,
                is_deleted: false,
                a: 1 },
              { salesforce_id: 'sf_id_1',
                updated_by: 'any',
                sync_version: DateTime.iso8601('2001-02-01T02:01:01+01:00'),
                is_deleted: false,
                is_synced: true }
            ]

            # Copy required params
            saved_entities = create_entities
            params[:data].map.with_index do |el, i|
              required_params.each do |key|
                el[key.to_sym] = saved_entities[i][key.to_sym]
              end
              el
            end

            # clear db
            model_type.limit(3).destroy
            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.header 'Content-Type', 'application/json'
            browser.post "#{api_path}/bulk_update", params.to_json

            expect(browser.last_response.status).to eq(200)

            entities = model_type.all
            expect(entities.size).to eq 2

            el = entities[0]
            request_data = params[:data][0]
            expect(el.updated_by).to eq('api')
            expect(el.salesforce_id).to eq(request_data[:salesforce_id])
            expect(el.is_synced).to eq(false)
            expect(el.updated_at.nil?).to eq(false)
            expect(el.sync_version > request_data[:sync_version]).to eq(true)

            el = entities[1]
            request_data = params[:data][1]
            expect(el.updated_by).to eq('api')
            expect(el.salesforce_id).to eq(request_data[:salesforce_id])
            expect(el.is_synced).to eq(false)
            expect(el.updated_at.nil?).to eq(false)
            expect(el.sync_version > request_data[:sync_version]).to eq(true)
          end

          it 'update 3' do
            saved_entities = create_entities

            saved_entities[0].salesforce_id = 'update_sf_id_1'
            saved_entities[0].updated_by = 'salesforce'
            saved_entities[0].is_synced = false
            saved_entities[0].sync_version = DateTime.iso8601('2001-01-01T02:01:01+01:00')
            saved_entities[0].should_not_set_sync_arguments = true
            saved_entities[0].save

            saved_entities[1].salesforce_id = 'update_sf_id_2'
            saved_entities[1].updated_by = 'salesforce'
            saved_entities[1].is_synced = true
            saved_entities[1].sync_version = DateTime.iso8601('2001-01-01T02:01:01+01:00')
            saved_entities[1].should_not_set_sync_arguments = true
            saved_entities[1].save

            saved_entities[2].salesforce_id = 'update_sf_id_3'
            saved_entities[2].updated_by = 'salesforce'
            saved_entities[2].is_synced = true
            saved_entities[2].sync_version = DateTime.iso8601('2001-01-01T02:01:01+01:00')
            saved_entities[2].should_not_set_sync_arguments = true
            saved_entities[2].save

            params = {}
            params[:data] = [
              {
                salesforce_id: saved_entities[0].salesforce_id,
                sync_version: saved_entities[0].sync_version + 600,
                updated_by: 'sync_batcher',
                werbezeichen_sync_version: DateTime.now, # is_synced is missing (default: false)
                is_deleted: false,
                a: 1
              },
              {
                salesforce_id: saved_entities[1].salesforce_id,
                sync_version: saved_entities[1].sync_version, # sync even if sync_version is equal
                updated_by: 'sf',
                werbezeichen_sync_version: DateTime.now,
                is_deleted: false,

                is_synced: true # set is_synced to true
              },
              {
                salesforce_id: saved_entities[2].salesforce_id,
                sync_version: saved_entities[2].sync_version + 300,
                updated_by: 'sf_updater',
                werbezeichen_sync_version: DateTime.now,
                is_deleted: false,
                is_synced: true
              }
            ]

            params[:data].map.with_index do |el, i|
              required_params.each do |key|
                el[key.to_sym] = saved_entities[i][key.to_sym]
              end
              el
            end

            before_upsert_entities = model_type.all
            expect(!before_upsert_entities.empty?).to eq(true)
            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.header 'Content-Type', 'application/json'
            browser.post "#{api_path}/bulk_update", params.to_json

            expect(browser.last_response.status).to eq(200)

            entities = model_type.all
            expect(entities.size).to eq saved_entities.size

            el = entities[0]
            expect(el.id).to eq(saved_entities[0].id)
            expect(el.updated_by).to eq('sync_batcher')
            expect(el.is_synced).to eq(false)
            expect(el.sync_version > saved_entities[0].sync_version).to eq(true)

            el = entities[1]
            expect(el.id).to eq(saved_entities[1].id)
            expect(el.updated_by).to eq('sf')
            expect(el.is_synced).to eq(true)
            expect(el.sync_version == saved_entities[1].sync_version).to eq(true)

            el = entities[2]
            expect(el.id).to eq(saved_entities[2].id)
            expect(el.updated_by).to eq('sf_updater')
            expect(el.is_synced).to eq(true)
            expect(el.sync_version > saved_entities[2].sync_version).to eq(true)
          end

          it 'delete 2 from salesforce' do
            saved_entities = create_entities

            saved_entities[0].salesforce_id = 'sf_id_1'
            saved_entities[0].updated_by = 'salesforce'
            saved_entities[0].is_synced = true
            saved_entities[0].sync_version = Time.now
            saved_entities[0].should_not_set_sync_arguments = true
            saved_entities[0].save

            saved_entities[1].updated_by = 'salesforce'
            saved_entities[1].is_synced = true
            saved_entities[1].sync_version = Time.now
            saved_entities[1].should_not_set_sync_arguments = true
            saved_entities[1].save

            params = {}
            params[:data] = [
              {
                salesforce_id: saved_entities[0].salesforce_id,
                sync_version: saved_entities[0].sync_version + 60,
                updated_by: 'salesforce',
                is_synced: true,
                is_deleted: true,
                werbezeichen_sync_version: Time.now,
                a: 1
              },
              {
                id: saved_entities[1].id,
                sync_version: saved_entities[1].sync_version + 20,
                updated_by: 'salesforce',
                is_synced: true,
                is_deleted: true,
                a: 1
              }
            ]

            # Copy required params
            params[:data].map.with_index do |el, i|
              required_params.each do |key|
                el[key.to_sym] = saved_entities[i][key.to_sym]
              end
              el
            end

            before_upsert_entities = model_type.all
            expect(!before_upsert_entities.empty?).to eq(true)
            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.header 'Content-Type', 'application/json'
            browser.post "#{api_path}/bulk_update", params.to_json

            expect(browser.last_response.status).to eq(200)

            entities = model_type.all
            expect(entities.size).to eq 1 # Only 2 are deleted
          end

          it 'upsert > insert 1, update 1 and delete 1' do
            saved_entities = create_entities

            saved_entities[0].salesforce_id = 'upsert_sf_id_0'
            saved_entities[0].is_synced = true
            saved_entities[0].sync_version = Time.now
            saved_entities[0].should_not_set_sync_arguments = true
            saved_entities[0].save

            saved_entities[1].salesforce_id = 'sf_id_1'
            saved_entities[1].updated_by = 'salesforce'
            saved_entities[1].is_synced = true
            saved_entities[1].sync_version = Time.now
            saved_entities[1].should_not_set_sync_arguments = true
            saved_entities[1].save

            params = {}
            params[:data] = [
              {
                salesforce_id: saved_entities[0].salesforce_id,
                sync_version: saved_entities[0].sync_version + 20,
                updated_by: 'salesforce', is_synced: true,
                werbezeichen_sync_version: DateTime.now,
                is_deleted: false,
                a: 1
              },
              {
                salesforce_id: 'new_entity',
                sync_version: DateTime.iso8601('2001-01-01T02:01:01+01:00'),
                is_synced: true, # missing werbezeichen_sync_version --> is_synced = false
                a: 1
              },
              {
                salesforce_id: saved_entities[1].salesforce_id,
                sync_version: saved_entities[1].sync_version + 20,
                updated_by: 'salesforce', is_synced: true,
                is_deleted: true,
                a: 1
              }
            ]

            # Copy required params
            params[:data].map.with_index do |el, i|
              required_params.each do |key|
                el[key.to_sym] = saved_entities[i][key.to_sym]
              end
              el
            end

            before_upsert_entities = model_type.all
            expect(!before_upsert_entities.empty?).to eq(true)
            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.header 'Content-Type', 'application/json'
            browser.post "#{api_path}/bulk_update", params.to_json

            expect(browser.last_response.status).to eq(200)

            entities = model_type.all

            expect(entities.size).to eq saved_entities.size # 1 deleted 1 added

            el = entities[entities.size - 1] # last is new inserted entity
            expect(el.salesforce_id).to eq('new_entity')
            expect(el.updated_by).to eq('api')
            expect(el.is_synced).to eq(false)
            expect(el.sync_version > params[:data][1][:sync_version]).to eq(true)

            el = entities[0]
            expect(el.updated_by).to eq('salesforce')
            expect(el.is_synced).to eq(true)
            expect(el.sync_version > saved_entities[0][:sync_version]).to eq(true)
          end

          it 'update 1 > Do update not if sync_version is outdated' do
            saved_entities = create_entities
            entity_to_update = saved_entities.first

            entity_to_update.salesforce_id = nil
            entity_to_update.updated_by = 'api'
            entity_to_update.is_synced = true
            entity_to_update.sync_version = DateTime.iso8601('2001-02-01T02:01:01+01:00')
            entity_to_update.should_not_set_sync_arguments = true
            entity_to_update.save

            params = {}
            params[:data] = [
              { id: entity_to_update.id,
                sync_version: DateTime.iso8601('2001-01-01T01:01:01+01:00'),
                salesforce_id: 'new_sf_id',
                entity_to_update: 'salesforce',
                is_synced: true,
                is_deleted: false,
                werbezeichen_sync_version: DateTime.now }
            ]

            # Copy required params
            params[:data].map.with_index do |el, i|
              required_params.each do |key|
                el[key.to_sym] = saved_entities[i][key.to_sym]
              end
              el
            end

            before_upsert_entities = model_type.all
            expect(!before_upsert_entities.empty?).to eq(true)
            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.header 'Content-Type', 'application/json'
            browser.post "#{api_path}/bulk_update", params.to_json

            expect(browser.last_response.status).to eq(200)

            entities = model_type.all
            expect(entities.size).to eq saved_entities.size

            entities.each do |el|
              next unless el.id == entity_to_update.id

              expect(el.updated_by).to eq('api')
              expect(el.salesforce_id == entity_to_update.salesforce_id).to eq(true)
              expect(el.sync_version == entity_to_update.sync_version).to eq(true)
            end
          end

          it 'update 1 > Do update by id and with same sync_version' do
            saved_entities = create_entities
            entity_to_update = saved_entities.first

            entity_to_update.salesforce_id = nil
            entity_to_update.updated_by = 'api'
            entity_to_update.is_synced = true
            entity_to_update.sync_version = DateTime.iso8601('2001-02-01T02:01:01+01:00')
            entity_to_update.should_not_set_sync_arguments = true
            entity_to_update.save

            params = {}
            params[:data] = [
              { id: entity_to_update.id,
                sync_version: entity_to_update.sync_version,
                salesforce_id: 'new_sf_id',
                entity_to_update: 'salesforce',
                is_synced: true,
                is_deleted: false,
                werbezeichen_sync_version: DateTime.now }
            ]

            # Copy required params
            params[:data].map.with_index do |el, i|
              required_params.each do |key|
                el[key.to_sym] = saved_entities[i][key.to_sym]
              end
              el
            end

            before_upsert_entities = model_type.all
            expect(!before_upsert_entities.empty?).to eq(true)
            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.header 'Content-Type', 'application/json'
            browser.post "#{api_path}/bulk_update", params.to_json

            expect(browser.last_response.status).to eq(200)

            entities = model_type.all
            expect(entities.size).to eq saved_entities.size

            entities.each do |el|
              next unless el.id == entity_to_update.id

              expect(el.updated_by).to eq('api')
              expect(el.salesforce_id == 'new_sf_id').to eq(true)
              expect(el.sync_version == entity_to_update.sync_version).to eq(true)
            end
          end
        end
      end

      def test_pdf_update_endpoint
        api_path = @api_path
        model_type = @model_type
        app = @app

        @spec_instance.describe "POST #{api_path}/bulk_get" do
          before do
            allow_any_instance_of(Aws::SQS::Client)
              .to receive(:create_queue)
              .and_return(OpenStruct.new(queue_url: 'queue_url'))
            allow_any_instance_of(WzSalesforceSync::SqsQueueAdapter).to receive(:send_usecase_to_queue)
            # allow_any_instance_of(WzSalesforceSync::SqsQueueAdapter).to receive(:send_usecase_to_queue)
          end

          it 'update public_s3_path' do
            entities = create_entities
            entity = entities[0]
            entity.salesforce_id = 'sync_sf_id_0'
            entity.save

            params = { public_s3_path: 'new_path/abc' }

            browser = Rack::Test::Session.new(Rack::MockSession.new(app))
            browser.post "#{api_path}/#{entity.salesforce_id}/update_pdf", params
            expect(browser.last_response.status).to eq(200)
            saved_entities = model_type.find(salesforce_id: entity.salesforce_id)
            expect(saved_entities.public_s3_path).to eq(params[:public_s3_path])
          end
        end
      end
    end
  end
end
