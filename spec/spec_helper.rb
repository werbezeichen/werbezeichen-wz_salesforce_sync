# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'
ENV['AUTH_SERVICE_HOST_URL'] = 'test.com'
require 'simplecov'
SimpleCov.start

require 'bundler/setup'
require 'wz_salesforce_sync'
require 'sqlite3'
require 'sequel'
require 'rspec'
require 'database_cleaner'
require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: true)

AWS_ACCESS_KEY_ID = 'id'
AWS_SECRET_ACCESS_KEY = 'key'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before do
    DatabaseCleaner.strategy = :deletion
    stub_request(:any, /test.com/)
  end

  config.before do
    DatabaseCleaner.start
  end

  config.after do
    DatabaseCleaner.clean
  end
end

Sequel::Model.db = Sequel.sqlite('test.db')
Sequel::Model.plugin :validation_helpers
SALESFORCE_SERVICE_QUEUE_NAME = 'queue'
# Open a database
db = SQLite3::Database.new('test.db')
# Create a table with sync base fields
db.execute <<-SQL
  CREATE TABLE IF NOT EXISTS mock_sequels (
    id INTEGER PRIMARY KEY,
    uuid varchar(30),
    name varchar(30),
    salesforce_id varchar(30),
    updated_at datetime,
    sync_version datetime,
    updated_by varchar(30),
    is_synced boolean,
    is_deleted boolean,
    url_key varchar(30),
    public_s3_path varchar(30)
  );
SQL
# db.execute <<-SQL
#   DROP TABLE  mock_sequels;
# SQL

module Sequel
  class Model
    alias save! save
    plugin :dirty
  end
end
class MockSequel < Sequel::Model
  include WzSalesforceSync::SalesforceSyncable

  dataset_module do
    # SQLite specific methods, for dev/test purpose only
    def on_duplicate_key_update(*_keys)
      insert_conflict(:replace)
    end

    def multi_insert(rows)
      rows.each do |row|
        insert(row)
      end
    end
  end
end

module API
  module Entities
    module Sync
      class MockSequel < WzSalesforceSync::GrapeSyncEntity
        expose :url_key
      end
    end
  end
end
