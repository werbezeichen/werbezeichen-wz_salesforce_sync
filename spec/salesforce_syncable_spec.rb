# frozen_string_literal: true

RSpec.describe WzSalesforceSync::SalesforceSyncable do
  context do
    it 'returns the sync resource name' do
      expect(MockSequel.sync_resource_name).to eq('mock_sequels')
    end
  end

  context do
    before do
      allow_any_instance_of(Aws::SQS::Client)
        .to receive(:create_queue)
        .and_return(OpenStruct.new(queue_url: 'queue_url'))
      allow_any_instance_of(WzSalesforceSync::SqsQueueAdapter).to receive(:send_usecase_to_queue)
    end

    it 'saves the record' do
      expect_any_instance_of(WzSalesforceSync::SqsQueueAdapter).to receive(:send_usecase_to_queue)
      mock_record = MockSequel.new(name: 'test').save
      expect(mock_record.is_synced).to eq(false)
      expect(mock_record.name).to eq('test')
      expect(mock_record.salesforce_id).to be_nil
      expect(mock_record.updated_by).to eq('api')
    end

    it 'updates the record' do
      mock_record = MockSequel.create(name: 'test')
      expect(mock_record.update(name: 'Awesome'))
      expect(mock_record.is_synced).to eq(false)
      expect(mock_record.name).to eq('Awesome')
      expect(mock_record.salesforce_id).to be_nil
      expect(mock_record.updated_by).to eq('api')
    end

    it 'bulk updates entities' do
      mock_record = MockSequel.create(name: 'test')
      mock_record[:name] = 'new name'
      MockSequel.bulk_sync(request_entities: [mock_record], keys: [], sync_after_upsert: true)

      expect(mock_record.reload.name).to eq 'new name'
    end

    it 'bulk inserts entities' do
      mock_record = MockSequel.new(name: 'inserted')
      MockSequel.bulk_sync(request_entities: [mock_record], keys: [], sync_after_upsert: true)

      expect(MockSequel.find(name: 'inserted')).to be_truthy
    end

    it 'bulk deletes entities' do
      mock_record = MockSequel.create(name: 'test')
      mock_record[:is_deleted] = true
      MockSequel.bulk_sync(request_entities: [mock_record], keys: [], sync_after_upsert: true)

      expect(mock_record.exists?).to be_falsey
    end
  end
end
