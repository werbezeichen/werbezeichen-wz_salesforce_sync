# WzSalesforceSync

**!! Versioning is not yet supported, we need to have a gem server manager in order to handle versioning correctly. Be careful that any change here will affect the builds on the APIs where the gem is used. !!**


Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/wz-salesforce-sync`. To experiment with that code, run `bin/console` for an interactive prompt.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'wz-salesforce-sync'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install wz-salesforce-sync

## Usage

Models using the sync **MUST** include the base fields:
  - id
  - name
  - salesforce_id
  - updated_at
  - sync_version (datetime)
  - updated_by
  - is_synced (boolean)
  - is_deleted (boolean)

Include the sync module to use its methods in sequel models:
```ruby
include WzSalesforceSync::SalesforceSyncable
```

To define an entity:
```ruby
module API
  module Entities
    module Sync
      class ModelSequel < WzSalesforceSync::GrapeSyncEntity
        expose :field, documentation: { param: { type: String } }
      end
    end
  end
end
```

To add default sync routes for an entity, create an endpoint inheriting from SyncRoutes and init default routes
```ruby
module API
  module Sync
    class ModelSequel < WzSalesforceSync::SyncRoutes
      init_default_routes(
        service: 'product_api', model_type: ModelSequel, entity_class: API::Entities::Sync::ModelSequel
      )
    end
  end
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bundle exec rspec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

`gem build wz-salesforce-sync.gemspec` to build the gem locally (creates the .gem file). Do not forget to change the `path` in your application using the gem to use the local path.
