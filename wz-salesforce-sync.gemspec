# frozen_string_literal: true

require_relative 'lib/wz_salesforce_sync/version'

Gem::Specification.new do |spec|
  spec.required_ruby_version = Gem::Requirement.new('>= 2.7.1')
  spec.name          = 'wz-salesforce-sync'
  spec.version       = WzSalesforceSync::VERSION
  spec.authors       = ['Bastien Barbé']
  spec.email         = ['bastien.barbe@werbezeichen.de']
  spec.summary       = 'Includes all the Saleforce Synchronization logic for Ruby/Grape applications.'
  spec.homepage      = 'https://bitbucket.org/werbezeichen/werbezeichen-wz_salesforce_sync'
  spec.license       = 'Nonstandard'
  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler-audit', '~> 0.6.1'
  spec.add_development_dependency 'database_cleaner', '~> 1.8.4'
  spec.add_development_dependency 'grape', '~> 1.3.2'
  spec.add_development_dependency 'grape-entity', '~> 0.8.0'
  spec.add_development_dependency 'rack-test', '~> 1.1.0'
  spec.add_development_dependency 'rspec', '~> 3.9.0'
  spec.add_development_dependency 'rubocop', '~> 0.84.0'
  spec.add_development_dependency 'sequel', '~> 5.31.0'
  spec.add_development_dependency 'simplecov', '~> 0.16.1'
  spec.add_development_dependency 'sqlite3', '~> 1.4.2'
  spec.add_development_dependency 'webmock', '~> 3.4.2'

  spec.add_runtime_dependency 'aws-sdk-sqs', '~> 1.24.0'
  spec.add_runtime_dependency 'faraday', '~> 1.0.1'
  spec.add_runtime_dependency 'grape', '~> 1.3.2'
  spec.add_runtime_dependency 'grape-entity', '~> 0.8.0'
  spec.add_runtime_dependency 'mysql2', '~> 0.5.3'
  spec.add_runtime_dependency 'require_all', '~> 1.3.3'
  spec.add_runtime_dependency 'sequel', '~> 5.31.0'
end
