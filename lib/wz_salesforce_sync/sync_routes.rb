# frozen_string_literal: true

require 'grape'
require 'grape-entity'
require 'require_all'
require_rel './helpers/'

module WzSalesforceSync
  class SyncRoutes < Grape::API
    format :json

    def self.init_default_routes(service:, model_type:, entity_class:, resource_name: nil,
                                 auth_helper: WzSalesforceSync::AuthHelper)
      helpers auth_helper
      helpers do
        WzSalesforceSync::EntityToParamsHelper.create_param(:entity_params, entity_class, self)
      end

      resource_name ||= (model_type.to_s.underscore + 's')
      resource resource_name do
        desc 'get by field_names', WzSalesforceSync::AuthHelper.auth_header
        params do
          requires :field_name, type: String
          requires :field_values, type: Array
          optional :negate, type: Boolean
        end
        post '/bulk_get' do
          verify_permissions('sync', resource_name.to_s, service)

          field_name = params[:field_name]
          field_values = params[:field_values].map do |value|
            if value == 'true'
              true
            elsif value == 'false'
              false
            else
              value.to_s
            end
          end
          negate = params[:negate]
          where_query = {}
          where_query[field_name.to_sym] = field_values
          where_query[:is_sync_disabled] = false if model_type.columns.include?(:is_sync_disabled)
          where_query = Sequel.negate(where_query) if negate
          result = model_type.where(where_query).all

          status 200
          present result, with: entity_class
        end

        desc 'bulk update', WzSalesforceSync::AuthHelper.auth_header
        params do
          requires :data, type: Array do
            use :entity_params
          end
          optional :sync_after_upsert, type: Boolean
        end
        post '/bulk_update' do
          verify_permissions('sync', resource_name.to_s, service)
          declared_params = declared(params, include_missing: true)

          begin
            model_type.bulk_sync(
              request_entities: declared_params[:data],
              sync_after_upsert: declared_params[:sync_after_upsert] ||= true
            )
          rescue Sequel::ValidationFailed => e
            puts e
            error!("An error occured during update: #{e}", 400)
          end

          status 200
        end
      end
    end

    def self.init_update_pdf_route(service:, model_type:, entity_class:, resource_name: nil)
      helpers WzSalesforceSync::AuthHelper
      helpers do
        EntityToParamsHelper.create_param(:entity_params, entity_class, self)
      end

      resource_name ||= (model_type.to_s.underscore + 's')
      resource resource_name do
        desc 'Update public_s3_path field', WzSalesforceSync::AuthHelper.auth_header
        params do
          requires :public_s3_path, type: String
        end
        post '/:salesforce_id/update_pdf' do
          verify_permissions('sync', resource_name.to_s, service)
          salesforce_id = params[:salesforce_id]

          entity = model_type.find(salesforce_id: salesforce_id)
          error!('Entity not found', 404) unless entity

          entity.should_not_set_sync_arguments = true
          entity.public_s3_path = params[:public_s3_path]

          entity.save

          status 200
        end
      end
    end
  end
end
