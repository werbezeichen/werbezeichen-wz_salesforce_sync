# frozen_string_literal: true

require 'grape'
require 'grape_entity'

module WzSalesforceSync
  class GrapeSyncEntity < Grape::Entity
    expose :id, documentation: { param: { type: Integer } }
    expose :salesforce_id, documentation: { param: { type: String } }
    expose :updated_by, documentation: { param: { type: String } }
    expose :sync_version, documentation: { param: { type: DateTime } }
    expose :is_deleted, documentation: { param: { type: Grape::API::Boolean } }
    expose :is_synced, documentation: { param: { type: Grape::API::Boolean } }
    # sync_metadata (not stored to db but set on post)
    expose :werbezeichen_sync_version, documentation: { param: { type: DateTime } }
    # readonly:
    expose :sync_version, as: :werbezeichen_sync_version
    expose :updated_at
  end
end
