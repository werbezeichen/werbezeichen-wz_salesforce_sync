# frozen_string_literal: true

require 'active_support'

module WzSalesforceSync
  module SalesforceSyncable
    def self.included(base)
      base.extend ClassMethods
      base.send :include, InstanceMethods
    end

    module InstanceMethods
      attr_accessor :should_not_set_sync_arguments

      def validate
        super
        validates_unique :salesforce_id unless salesforce_id.nil?
      end

      def before_save
        super
        self[:updated_at] = Time.now
        self.class.sync_arguments(self) unless should_not_set_sync_arguments
      end

      def after_create
        super
        self.class.after_save_entities(entities: [self], method: 'insert')
      end

      def after_update
        super
        self.class.after_save_entities(entities: [self], method: 'update')
      end

      # read only sync fields
      def werbezeichen_sync_version
        self[:werbezeichen_sync_version]
      end

      # rubocop:disable Naming/PredicateName
      def is_entity_new
        self[:is_entity_new] || false
      end
      # rubocop:enable Naming/PredicateName
    end

    module ClassMethods
      def after_save_entities(entities:, sync_to_salesforce: true, method: 'insert')
        # Triggers (get changes with entities.column_changes)
        send_sync_to_salesforce_event(entities, method) if sync_to_salesforce
      end

      def sync_resource_name
        ActiveSupport::Inflector.pluralize(ActiveSupport::Inflector.underscore(name))
      end

      def bulk_sync(request_entities:, keys: [], sync_after_upsert: true)
        entities_to_upsert = []
        entities_to_delete = []
        saved_entities = saved_entities(keys, request_entities)
        request_entities.each do |request_entity|
          request_entity_cpy = request_entity.clone
          sync_arguments(request_entity)
          saved_entity = saved_entity(saved_entities, request_entity)
          next if saved_entity && saved_entity[:is_sync_disabled]

          entity_sync_version(request_entity_cpy, request_entity)
          if !saved_entity.nil? && should_delete_entity?(request_entity_cpy, request_entity)
            entities_to_delete.push(saved_entity.clone)
          end
          next unless should_upsert_entity?(request_entity_cpy, request_entity, saved_entity)

          entities_to_upsert.push(merged_entity(saved_entity, request_entity))
        end
        delete_entities(entities_to_delete)
        upsert_entities_and_sync(entities_to_upsert, sync_after_upsert, request_entities.size)
      end

      def send_sync_to_salesforce_event(entities_to_sync, method)
        changed_entities_to_sync = entities_to_sync.reject { |el| el[:is_synced] }
        ids = changed_entities_to_sync.map do |el|
          el[:uuid] || el[:id] || el[:salesforce_id]
        end.compact
        return if ids.empty?

        puts " > #{ids.size} entities are out of sync" unless ENV['RACK_ENV'] == 'test'
        delay = method == 'insert' ? 180 : 1
        queue = WzSalesforceSync::SqsQueueAdapter.new(SALESFORCE_SERVICE_QUEUE_NAME, delay)
        queue.send_usecase_to_queue('SalesforceSync', { wz_entity_name: sync_resource_name, ids: ids })
      end

      # TODO: specs
      def upload_pdfs(entities, method)
        entities.each do |el|
          should_upload = el.public_s3_path.nil? && !el.pdf_public_url.nil?

          if !should_upload && method == 'update'
            pdf_changes = el.column_changes[:pdf_public_url]
            should_upload = !pdf_changes.nil?
          end

          dispatch_upload_pdf_task(el.salesforce_id) if should_upload
        end
      end

      def sync_arguments(element)
        element[:updated_at] = Time.now
        element[:sync_version] = element[:updated_at]
        element[:updated_by] = 'api'
        element[:is_synced] = false
      end

      private

      def delete_entities(entities_to_delete)
        return if entities_to_delete.empty?

        ids = entities_to_delete.map { |el| el[:id] }.compact
        salesforce_ids = entities_to_delete.map { |el| el[:salesforce_id] }.compact
        uuids = entities_to_delete.map { |el| el[:uuid] }.compact
        dataset.where(id: ids).or(salesforce_id: salesforce_ids).or(uuid: uuids).delete
      end

      def merged_entity(saved_entity, request_entity)
        merged_entity = new({})

        saved_entity.nil? ? merged_entity[:is_entity_new] = true : merged_entity = saved_entity
        # set values
        request_entity.each do |key, value|
          next if key.to_sym == :id

          merged_entity[key.to_sym] = value
        end
        merged_entity
      end

      def should_upsert_entity?(request_entity_cpy, request_entity, saved_entity)
        # upsert if: should not delete entity and is new entity and not deleted, saved entity has no sync_version yet,
        # entity from request has no sync_version set, entity from request has a newer sync_version
        !should_delete_entity?(request_entity_cpy, request_entity) &&
          ((saved_entity.nil? && request_entity[:is_deleted] != true) ||
            (!saved_entity.nil? && saved_entity.sync_version <= request_entity[:sync_version]))
      end

      def should_delete_entity?(request_entity_cpy, request_entity)
        request_entity[:is_deleted] == true &&
          (request_entity[:is_synced] == true || request_entity_cpy[:werbezeichen_sync_version].nil?)
      end

      def entity_sync_version(request_entity_cpy, request_entity)
        if request_entity_cpy[:werbezeichen_sync_version].nil? # if saved_entity.nil?
          # Add one second to sync_version --> Triggers sync to SF
          # If sync_version is set to updated_at it might lead to a race condition between SF and WZ
          unless request_entity_cpy[:sync_version].nil?
            request_entity[:sync_version] = request_entity_cpy[:sync_version] + (1.0 / (24 * 60 * 60))
          end
        else
          request_entity[:updated_by] = request_entity_cpy[:updated_by] unless request_entity_cpy[:updated_by].nil?
          request_entity[:is_synced] = request_entity_cpy[:is_synced].nil? ? false : request_entity_cpy[:is_synced]
          unless request_entity_cpy[:sync_version].nil?
            request_entity[:sync_version] = request_entity_cpy[:sync_version]
          end
        end
      end

      def saved_entity(entities, request_entity)
        entities.detect do |saved_el|
          id_present(saved_el, request_entity[:id]) ||
            uuid_present(saved_el, request_entity[:uuid]) ||
            (!saved_el.salesforce_id.nil? && saved_el.salesforce_id == request_entity[:salesforce_id])
        end
      end

      def id_present(saved_el, request_entity_id)
        saved_el.columns.include?(:id) && !saved_el.id.nil? && saved_el.id == request_entity_id
      end

      def uuid_present(saved_el, request_entity_uuid)
        (saved_el.columns.include?(:uuid) && !saved_el.uuid.nil? && saved_el.uuid == request_entity_uuid)
      end

      def saved_entities(keys, request_entities)
        entities = dataset.where(id: []) # where (1 = 0) - returns empty set if no row found
        keys.push(:id)
        keys.push(:salesforce_id)
        keys.push(:uuid)
        keys.uniq!
        keys.each do |key|
          key_values = request_entities.map { |el| el[key] }.compact
          key_hash = {}
          key_hash[key] = key_values
          entities = entities.or(key_hash)
        end
        entities.all
      end

      def upsert_entities_and_sync(entities_to_upsert, sync_after_upsert, request_entities_size)
        return if entities_to_upsert.empty?

        inserted_entities = []
        updated_entities = []
        entities_to_upsert.map! do |el_to_upsert|
          el_to_upsert[:is_entity_new] ? inserted_entities.push(el_to_upsert) : updated_entities.push(el_to_upsert)
          el_to_upsert.values.reject { |key, _| read_only_sync_fields.include?(key.to_sym) }
        end
        # First entity in array sets Values that are upserted --> Must be one with most values
        dataset.on_duplicate_key_update(*entities_to_upsert[0].keys).multi_insert(entities_to_upsert)
        puts "Upserted #{entities_to_upsert.size} of #{request_entities_size}"

        inserted_entities.each_slice(10_000) do |batch|
          after_save_entities(entities: batch, sync_to_salesforce: sync_after_upsert, method: 'insert')
        end
        updated_entities.each_slice(10_000) do |batch|
          after_save_entities(entities: batch, sync_to_salesforce: sync_after_upsert, method: 'update')
        end
      end

      def dispatch_upload_pdf_task(salesforce_id)
        # return if ENV['RACK_ENV'] == 'test'

        upload_args = {
          werbezeichen_entity_name: sync_resource_name,
          salesforce_parent_id: salesforce_id
        }
        queue = WzSalesforceSync::SqsQueueAdapter.new(SALESFORCE_SERVICE_QUEUE_NAME)
        queue.send_usecase_to_queue('UploadFirstAttachedDocumentPdfToS3', upload_args)
      end

      def read_only_sync_fields
        %i[werbezeichen_sync_version is_entity_new]
      end
    end
  end
end
