# frozen_string_literal: true

require 'aws-sdk-sqs' # v3: require 'aws-sdk'

module WzSalesforceSync
  class SqsQueueAdapter
    attr_reader :queue_url, :sqs_client, :delay_seconds

    def initialize(queue_name, delay_seconds = 1)
      return if disabled?

      credentials = Aws::Credentials.new(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
      @sqs_client = Aws::SQS::Client.new(region: 'eu-central-1', credentials: credentials)
      @queue_url = sqs_client.create_queue(queue_name: queue_name).queue_url
      @delay_seconds = delay_seconds || 1
    end

    def disabled?
      ENV['DISABLE_SQS_QUEUE'] == 'true'
    end

    def send_usecase_to_queue(usecase_name, payload)
      message_string = { usecase_name: usecase_name, payload: payload }.to_json
      send_message(message_string)
    end

    def send_worker_task_to_queue(worker_task_name, options)
      message_string = { workerTaskName: worker_task_name, options: options }.to_json
      send_message(message_string)
    end

    private

    def send_message(msg_string)
      return if disabled?

      sqs_client.send_message({ queue_url: queue_url, message_body: msg_string, delay_seconds: delay_seconds })
    end
  end
end
