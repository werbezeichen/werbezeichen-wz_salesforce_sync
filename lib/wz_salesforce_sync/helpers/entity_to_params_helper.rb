# frozen_string_literal: true

module WzSalesforceSync
  module EntityToParamsHelper
    def self.create_param(name, entity_type, helper_instance)
      helper_instance.params name.to_sym do
        entity_type.root_exposures.each do |exposure|
          key = exposure.key

          next unless exposure.documentation && exposure.documentation[:param]

          param = exposure.documentation[:param]

          param_required = param[:required]
          param_type = param[:type]

          if param_required
            requires key, type: param_type
          else
            optional key, type: param_type
          end
        end
      end
    end
  end
end
