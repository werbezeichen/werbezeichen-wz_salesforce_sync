# frozen_string_literal: true

require 'faraday'

module WzSalesforceSync
  module AuthHelper
    extend Grape::API::Helpers

    def self.auth_header
      { headers: { Authorization: { description: 'Authorization Token' } } }
    end

    def verify_permissions(action, context, service)
      can_callout(action, context, service)
    end

    # rubocop:disable Naming/AccessorMethodName
    def get_user_accounts
      response = authservice_callout('/api/v2/accounts/my')
      JSON.parse(response.body)
    end

    def get_user_id
      response = authservice_callout('/api/v1/users/me')
      JSON.parse(response.body)['salesforce_id']
    end
    # rubocop:enable Naming/AccessorMethodName

    def user_uuid
      response = authservice_callout('/api/v1/users/me')
      JSON.parse(response.body)['uuid']
    end

    def accounts_users
      response = authservice_callout('/api/v1/users/me')
      JSON.parse(response.body)['accounts_users']
    end

    def accounts_user_backend_role
      accounts_users.detect { |au| au['account_uuid'] == params[:account_uuid] }['backend_role']
    end

    private

    def can_callout(action, context, service)
      body = { service: service, action: action, context: context }
      authservice_callout('/api/v1/users/can', body)
    end

    def authservice_callout(endpoint, body = {})
      response = Faraday.get(
        "#{ENV['AUTH_SERVICE_HOST_URL']}#{endpoint}",
        body,
        { authorization: env['Authorization'] || env['HTTP_AUTHORIZATION'] || '', x_service: ENV['SERVICE_NAME'] || '' }
      )
      response_errors(response)
      response
    end

    def response_errors(response)
      error!('Error, no response from auth service', 400) unless response
      error!('Not authorized', 401) if response.status == 401
      error!('Not allowed', 403) if response.status > 401
    end
  end
end
